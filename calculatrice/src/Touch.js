import React, { Component } from 'react';

const Touch = ({onClick, value}) => (
    <button className="touch" onClick={() => onClick(value)}>
        {value}
    </button>
)

export default Touch