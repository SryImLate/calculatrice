import React, { Component } from 'react';
import Touch from './Touch';

class Pad extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {

        return(
        <div>
            <div className="row">
                <Touch value="7" onClick={() => this.props.onClickNumber("7")}/>
                <Touch value="8" onClick={() => this.props.onClickNumber("8")}/>
                <Touch value="9" onClick={() => this.props.onClickNumber("9")}/>
                <Touch value="/" onClick={() => this.props.onClickOperation("/")}/>
            </div>
            <div className="row">
                <Touch value="4" onClick={() => this.props.onClickNumber("4")}/>
                <Touch value="5" onClick={() => this.props.onClickNumber("5")}/>
                <Touch value="6" onClick={() => this.props.onClickNumber("6")}/>
                <Touch value="X" onClick={() => this.props.onClickOperation("X")}/>
            </div>
            <div className="row">
                <Touch value="1" onClick={() => this.props.onClickNumber("1")}/>
                <Touch value="2" onClick={() => this.props.onClickNumber("2")}/>
                <Touch value="3" onClick={() => this.props.onClickNumber("3")}/>
                <Touch value="-" onClick={() => this.props.onClickOperation("-")}/>
            </div>
            <div className="row">
                <Touch value="0" onClick={() => this.props.onClickNumber("0")}/>
                <Touch value="AC" onClick={() => this.props.onClickAc()}/>
                <Touch value="," onClick={() => this.props.onClickNumber(".")}/>
                <Touch value="+" onClick={() => this.props.onClickOperation("+")}/>
                <Touch value="=" onClick={() => this.props.onClickEqual()}/>
            </div>        
        </div>
        )
    }
}

export default Pad