import React from 'react';
import logo from './logo.svg';
import './App.css';
import Screen from './Screen';
import Pad from './Pad';
import Layout from './Layout'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Layout>
          <Screen />
          <Pad />
        </Layout>
      </header>
    </div>
  );
}

export default App;
