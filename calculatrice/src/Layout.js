import React, { Component } from 'react';
import Screen from './Screen';
import Pad from './Pad';

class Layout extends Component {
    constructor() {
        super()
        this.state = {
            firstOp: true,
            display: '',
            numbers: [],
            operations: [],
            waitingForNumber: true
        }
        this.onClickNumber = this.onClickNumber.bind(this)
        this.onClickOperation = this.onClickOperation.bind(this)
        this.onClickEqual = this.onClickEqual.bind(this)
        this.getDisplay = this.getDisplay.bind(this)
        this.onClickAc = this.onClickAc.bind(this)
        
    }

    onClickNumber(value) {
        var numbers = this.state.numbers
        if (value === ".") {
            if(!this.state.waitingForNumber) {
                var length = numbers.length
                if(numbers[length - 1].includes(".")) {
                    
                } else {
                    var num = numbers[length-1].concat(value)
                    numbers.splice(length-1, 1)
                    numbers.push(num)
                    this.setState({numbers: numbers})
                }
            }
        } else {
            if(this.state.waitingForNumber) {
                numbers.push(value);
                this.setState({numbers: numbers})
                this.setState({waitingForNumber: false})
            } else {
                var i = this.state.numbers.length
                var num = numbers[i-1].concat(value)
                numbers.splice(i-1, 1)
                numbers.push(num)
                this.setState({numbers: numbers})
            }
        }
        this.getDisplay()
    }
    getDisplay() {
        var numbers = this.state.numbers
        var op = this.state.operations
        var display=''
        if(op.length) {
            for(var n = 0; n<numbers.length; n++) {
                display = display.concat(numbers[n])
                if (n < op.length) {
                display = display.concat(op[n])
                    }
            }
        } else {
            display = display.concat(numbers[0])
        }
        this.setState({display: display})
        console.log(display)
        console.log(this.state)
    }
    


    onClickOperation(operation) {
        var operations = this.state.operations
        if((this.state.waitingForNumber) && (!this.state.operations.length)) {
        } else if (this.state.waitingForNumber) {
            var i = this.state.operations.length
            operations.splice(i-1, 1)
            operations.push(operation)
            this.setState({operations: operations})
        } else {
            operations.push(operation)
            this.setState({operations: operations})
            this.setState({waitingForNumber: true})
        }
        this.getDisplay()
    }


onClickEqual() {
    var num = this.state.numbers
    var op = this.state.operations
    var result
    if (op.length) {
        for(var i = 0; i < op.length; i++) {
            if (op[i] === 'X') {
                result = parseFloat(num[i])*parseFloat(num[i+1])
                num.splice(i, 2, result.toString())
                this.setState({numbers: num})
                op.splice(i, 1)
                this.setState({operations: op})
                i = i-1
            }
            if (op[i] === '/') {
                result = parseFloat(num[i])/parseFloat(num[i+1])
                num.splice(i, 2, result.toString())
                this.setState({numbers: num})
                op.splice(i, 1)
                this.setState({operations: op})
                i = i-1
            }
        }
        for(i = 0; i < op.length; i++) {
            if (op[i] === '+') {
                result = parseFloat(num[i])+parseFloat(num[i+1])
                num.splice(i, 2, result.toString())
                this.setState({numbers: num})
                op.splice(i, 1)
                this.setState({operations: op})
                i = i-1
            }
            if (op[i] === '-') {
                result = parseFloat(num[i])-parseFloat(num[i+1])
                num.splice(i, 2, result.toString())
                this.setState({numbers: num})
                op.splice(i, 1)
                this.setState({operations: op})
                i = i-1
            }
        }
        this.setState({display: num[0]})
    }
}

onClickAc() {
    this.setState({numbers: []})
    this.setState({operations: []})
    this.setState({display: ''})
    this.setState({waitingForNumber: true})
}


    render() {
        const { display } = this.state

        return(
            <div className="calcul">
                <Screen display={display}/>
                <Pad onClickAc={this.onClickAc} onClickNumber={this.onClickNumber} onClickOperation={this.onClickOperation} onClickEqual={this.onClickEqual}/>
            </div>
        )
    }
}

export default Layout